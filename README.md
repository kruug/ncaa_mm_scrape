# NCAA_MM_Scrape[![pipeline status](https://gitlab.com/kruug/ncaa_mm_scrape/badges/master/pipeline.svg)](https://gitlab.com/kruug/ncaa_mm_scrape/commits/master)

## Screenshot

No screenshots, as the only output is a JSON file.

## Installation



## Requirements

To run, you will need to install python3 (Python 3.5 and 3.7 tested working) and [BeautifulSoup4](https://pypi.org/project/beautifulsoup4/).

Best accomplished by installing python3 and pip3 however your OS of choice installs packages, and then running `pip3 install -r requirements.txt`.

## Usage

Run `python3 bracket_scrape.py` and the script will output a JSON file that can be consumed in any way you would normally consume JSON files.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History/Changelog

- Check out [changelog](CHANGELOG.md)

## Future/To-Do

- Add team logos to JSON.
- Test with live data instead of archive data.

### Additional wish-list

- Better documentation, both within the file as well as in the repository.

## License
NOTE: Only the code is covered by this license.  NCAA data is owned by the NCAA and used without explicit permission.

The MIT License (MIT)

Copyright (c) 2019 Viktor Kruug

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.