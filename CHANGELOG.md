# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2019-02-26]

### Added

- Added team logos to all games and champion listings.

## [2019-02-21]
### Added
 - Properly processes all games programatically
 - Outputs programatically formatted JSON
 - Inclusion of `requirements.txt` ensures that user environment matches development environment

### Fixed
 - CI/CD now handled through containers and using the same Python version as local environment.

### Changed
 - Round data is now parsed out from a list to a dictionary instead of manually forming JSON.

### Removed
 - Removed testing/debugging code
 - Removed duplicate round parsing

## [2019-02-19]
### Added
 - Script properly scrapes Archive 2018 NCAA March Madness bracket.
 - Manually processes First Round games
 - Outputs manually formatted JSON
 - Added README and CI/CD script
 - Added license file

### Fixed
 - Nothing to fix, initial commit

### Changed
 - Nothing changed, initial commit

### Removed
 - Nothing removed, initial commit
