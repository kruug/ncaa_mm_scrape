from bs4 import BeautifulSoup
import urllib.request
import io
import json

# Specify the url
# Using archive.org for testing.  Once the script is "finished", change to
# live data for actual use.
bracket_page = 'https://web.archive.org/web/20181001093517/https://www.ncaa.com/brackets/basketball-men/d1'

# Build the request, using a known robot useragent to hopefully not be blocked
# by the NCAA from scraping their page.
user_agent = 'Mozilla/5.0 (compatible; MJ12bot/v1.4.5; http://www.majestic12.co.uk/bot.php?+)'
req = urllib.request.Request(bracket_page, headers={'User-Agent': user_agent})

# Query the website and return the html to the variable 'page'
page = urllib.request.urlopen(req)

soup = BeautifulSoup(page, 'html.parser')

# First Four
ff_region = soup.find('div', attrs={'class':'first-four'})
ff_region = BeautifulSoup(str(ff_region), 'html.parser')
first_four = []
first_four.append(ff_region.find('div', attrs={'class':'game game-1'}))
first_four.append(ff_region.find('div', attrs={'class':'game game-2'}))
first_four.append(ff_region.find('div', attrs={'class':'game game-3'}))
first_four.append(ff_region.find('div', attrs={'class':'game game-4'}))

# First Four    101-104
# Top Left      201-208 301-304 401-402
# Bottom Left   209-216 305-308 403-404
# Top Right     217-224 309-312 405-406
# Bottom Right  225-232 313-316 407-408

# NCAA Website has the regions split up into top and bottom sections
# with championship game in between.  This code combines the two
# region sections into one for easier parsing.
regions = soup.findAll('div', attrs={'class':'regions'})
regions = str(regions[0]) + str(regions[1])
region = BeautifulSoup(regions, 'html.parser')

final = soup.find('div', attrs={'class':'center-final-games'})
#final = BeautifulSoup(final, 'html.parser')

# Intialize each list.
first_round = []
second_round = []
sweet_16 = []
elite_8 = []
final_4 = []
championship = []

# Scrap each round into their respective lists
# This is done to make filtering and output easier
for y in range(201, 233):
  first_round.append(region.find('div', attrs={'id':y}))

for y in range(301, 317):
  second_round.append(region.find('div', attrs={'id':y}))

for y in range(401, 409):
  sweet_16.append(region.find('div', attrs={'id':y}))

for y in range(501, 505):
  elite_8.append(region.find('div', attrs={'id':y}))

for y in range(601, 603):
  final_4.append(final.find('div', attrs={'id':y}))

championship.append(final.find('div', attrs={'id':'701'}))

champion = soup.find('div', attrs={'class':'champion'})
champ_team = champion.find('div', attrs={'class':'team-logo'}).find('span').text.strip()
champ_logo = champion.find('div', attrs={'class':'team-logo'}).find('img')['src'].strip()

scraped_games = {}

for game in first_four:
    scraped_games[f'game_'+game.find('div', attrs={'class':'game-pod'}).get('id')] = {
        'id': game.find('div', attrs={'class':'game-pod'}).get('id'),
        'status': game.find('span', attrs={'class':'state'}).text.strip(),
        'team_a': {
          'seed': game.findAll('div','team')[0].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[0].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[0].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[0].find('img', attrs={'class':'logo'})['src'].strip()
        },
        'team_b': {
          'seed': game.findAll('div','team')[1].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[1].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[1].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[1].find('img', attrs={'class':'logo'})['src'].strip()
        }
    } 

for game in first_round:
    scraped_games[f'game_'+game.get('id')] = {
        'id': game.get('id'),
        'status': game.find('span', attrs={'class':'state'}).text.strip(),
        'team_a': {
          'seed': game.findAll('div','team')[0].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[0].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[0].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[0].find('img', attrs={'class':'logo'})['src'].strip()
        },
        'team_b': {
          'seed': game.findAll('div','team')[1].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[1].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[1].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[1].find('img', attrs={'class':'logo'})['src'].strip()
        }
    }

for game in second_round:
    scraped_games[f'game_'+game.get('id')] = {
        'id': game.get('id'),
        'status': game.find('span', attrs={'class':'state'}).text.strip(),
        'team_a': {
          'seed': game.findAll('div','team')[0].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[0].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[0].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[0].find('img', attrs={'class':'logo'})['src'].strip()
        },
        'team_b': {
          'seed': game.findAll('div','team')[1].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[1].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[1].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[1].find('img', attrs={'class':'logo'})['src'].strip()
        }
    }

for game in sweet_16:
    scraped_games[f'game_'+game.get('id')] = {
        'id': game.get('id'),
        'status': game.find('span', attrs={'class':'state'}).text.strip(),
        'team_a': {
          'seed': game.findAll('div','team')[0].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[0].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[0].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[0].find('img', attrs={'class':'logo'})['src'].strip()
        },
        'team_b': {
          'seed': game.findAll('div','team')[1].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[1].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[1].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[1].find('img', attrs={'class':'logo'})['src'].strip()
        }
    }

for game in elite_8:
    scraped_games[f'game_'+game.get('id')] = {
        'id': game.get('id'),
        'status': game.find('span', attrs={'class':'state'}).text.strip(),
        'team_a': {
          'seed': game.findAll('div','team')[0].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[0].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[0].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[0].find('img', attrs={'class':'logo'})['src'].strip()
        },
        'team_b': {
          'seed': game.findAll('div','team')[1].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[1].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[1].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[1].find('img', attrs={'class':'logo'})['src'].strip()
        }
    }

for game in final_4:
    scraped_games[f'game_'+game.get('id')] = {
        'id': game.get('id'),
        'status': game.find('span', attrs={'class':'state'}).text.strip(),
        'team_a': {
          'seed': game.findAll('div','team')[0].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[0].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[0].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[0].find('img', attrs={'class':'logo'})['src'].strip()
        },
        'team_b': {
          'seed': game.findAll('div','team')[1].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[1].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[1].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[1].find('img', attrs={'class':'logo'})['src'].strip()
        }
    }

for game in championship:
    scraped_games[f'game_'+game.get('id')] = {
        'id': game.get('id'),
        'status': game.find('span', attrs={'class':'state'}).text.strip(),
        'team_a': {
          'seed': game.findAll('div','team')[0].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[0].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[0].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[0].find('img', attrs={'class':'logo'})['src'].strip()
        },
        'team_b': {
          'seed': game.findAll('div','team')[1].find('span', attrs={'class':'seed'}).text.strip(),
          'name': game.findAll('div','team')[1].find('span', attrs={'class':'name'}).text.strip(),
          'score': game.findAll('div','team')[1].find('span', attrs={'class':'score'}).text.strip(),
          'logo': game.findAll('div','team')[1].find('img', attrs={'class':'logo'})['src'].strip()
        }
    }

scraped_games[f'champion'] = {
  'name': champ_team,
  'logo': champ_logo
}

with open('bracket.json', 'w') as fd:
    fd.write(json.dumps(scraped_games, indent=4, sort_keys=True))
